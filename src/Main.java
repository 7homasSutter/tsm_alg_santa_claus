import Algorithmus.FirstOrder;
import Model.*;
import Util.CsvReader;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class Main {

    /*
    For this competition, you are asked to optimize the total weighted distance traveled (weighted reindeer weariness).
    You are given a list of gifts with their destinations and their weights. You will plan sleigh trips to deliver
    all the gifts to their destinations while optimizing the routes.

    All sleighs start from north pole, then head to each gift in the order that you assign, and then head back to
    north pole.
    You may have an unlimited number of sleigh trips.
    All the gifts must be traveling with the sleigh at all times until the sleigh delivers it to the destination.
    A gift may not be dropped off anywhere before it's delivered.
    Sleighs have a base weight of 10, and a maximum weight capacity of 1000 (excluding the sleigh).
    All trips are flying trips, which means you don't need to travel via land. Haversine is used in calculating
    distance.
     */
    public static void main(String[] args) {
        String csvFilePath = "/Users/tom/Documents/MSE/TSM_Alg/santa/data/gifts.csv";
        // Get Data
        ArrayList<Gift> gifts = CsvReader.createGifts(csvFilePath);
        double totalWeight = getTotalWeight(gifts);
        System.out.println("Number of Gifts in Dataset: " + gifts.size());

        // TODO Calc Solution - Minimizing --> Weighted Reindeer Weariness = (distance traveled) * (weights carried for that segment)
        // Example Random Solution
        Solution exampleSolution = FirstOrder.calcFirstOrderSolution(gifts);

        // TODO Compare Solution
        printSolution("Toms Random Solution", exampleSolution, totalWeight);
    }

    private static double getTotalWeight(ArrayList<Gift> gifts){
        double totalWeight=0;
        for(Gift gift: gifts){
            totalWeight += gift.getWeight();
            //System.out.println(gift.getId() + " Weigth "+gift.getWeight() + " Latitude "+ gift.getLatitude() + " Longitude "+ gift.getLongitude());
        }
        System.out.println("TotalWeight: " + totalWeight);
        return totalWeight;
    }

    private static void printSolution(String name, Solution solution, double totalWeight){
        System.out.println("\n\n"+name);
        System.out.println("\tNumber of Tours: " + solution.getTrips().size());
        System.out.println("\tSolution IsValid Northpole+AllGifts solution: " + solution.isValid());
        System.out.println("\tSolution Total Gifts: " + solution.getGiftCount() + " should exactly be " + 100000);
        System.out.println("\tSolution Total Weight: " + solution.getWeight() +" should exactly be "+ totalWeight);
        System.out.println("\tSolution Total Distance: " + solution.getDistance());
        System.out.println("\tSolution WeightedReindeerWeariness: " + solution.getWeightedReindeerWeariness());
        //System.out.println("\tSolution Mapping: " + solution.getSolutionMapping());
    }

    private static Solution calculateGoodSolution(){
        //TODO Calculate here a good solution
        return null;
    }


}
