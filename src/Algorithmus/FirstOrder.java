package Algorithmus;

import Model.*;

import java.util.ArrayList;

public class FirstOrder {
    /**
     * Calculates a solution from the order of gifts given.
     * @param gifts list of data
     * @return a bad solution for the santa claus problem
     */
    public static Solution calcFirstOrderSolution(ArrayList<Gift> gifts){
        ArrayList<Trip> tripList = new ArrayList<>();
        ArrayList<TripStep> currentTripStepList = new ArrayList<>();
        Sleigh sleigh = new Sleigh();
        Location northpole = new Location(Northpole.NORTHPOLE_LONGITUDE, Northpole.NORTHPOLE_LATITUDE);

        for(Gift gift: gifts){
            if(sleigh.getItemCount() == 0){
                startTrip(sleigh, gift, currentTripStepList, northpole);
            }
            else if(sleigh.hasSpace(gift)){
                sleigh.addItem(gift);
                int lastLocationIndex = currentTripStepList.size()-1;
                Location source = currentTripStepList.get(lastLocationIndex).getDestination();
                Location destination = new Location(gift.getLongitude(), gift.getLatitude());
                currentTripStepList.add(new TripStep(source, destination, gift));
            }else{
                endTrip(sleigh, currentTripStepList, northpole, tripList);
                clearTrip(sleigh, currentTripStepList);
                startTrip(sleigh, gift, currentTripStepList, northpole);
            }
        }
        endTrip(sleigh, currentTripStepList, northpole, tripList);
        return new Solution(tripList);
    }

    private static void clearTrip(Sleigh sleigh, ArrayList<TripStep> currentTripStepList){
        currentTripStepList.clear();
        sleigh.clearSleigh();
    }

    private static void startTrip(Sleigh sleigh, Gift gift, ArrayList<TripStep> currentTripStepList, Location northpole){
        sleigh.addItem(gift);
        Location destination = new Location(gift.getLongitude(), gift.getLatitude());
        currentTripStepList.add(new TripStep(northpole, destination, gift));
    }

    private static void endTrip(Sleigh sleigh, ArrayList<TripStep> currentTripStepList, Location northpole, ArrayList<Trip> tripList){
        int lastLocationIndex = currentTripStepList.size()-1;
        Location source = currentTripStepList.get(lastLocationIndex).getDestination();
        currentTripStepList.add(new TripStep(source, northpole));
        tripList.add(new Trip(new Sleigh(sleigh.getGifts()), currentTripStepList));
    }
}
