package Util;

import Model.Gift;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class CsvReader {

    public static ArrayList<Gift> createGifts(String csvFile){
        BufferedReader br = null;
        String line = "";
        String delimiter = ",";
        ArrayList<Gift> gifts = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(csvFile));
            br.readLine(); //Skip first line (headers)
            while ((line = br.readLine()) != null) {
                String[] giftData = line.split(delimiter);
                int id = Integer.valueOf(giftData[0]);
                double latitude = Double.valueOf(giftData[1]);
                double longitude = Double.valueOf(giftData[2]);
                double weight = Double.valueOf(giftData[3]);
                gifts.add(new Gift(id, latitude, longitude, weight));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return gifts;
    }
}
