package Model;

public class Gift {
    private int id;
    private double latitude;
    private double longitude;
    private double weight;

    public Gift(int id, double latitude, double longitude, double weight){
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getWeight() {
        return weight;
    }
}
