package Model;

import java.util.ArrayList;

public class Trip {
    private ArrayList<TripStep> tripSteps;
    private Sleigh sleigh;
    private double totalDistance;
    private double weightedReindeerWeariness;
    private static int tourCounter=0;
    private int id;

    public Trip(Sleigh sleigh, ArrayList<TripStep> tripSteps){
        this.tripSteps = tripSteps;
        this.sleigh = sleigh;
        this.totalDistance = calcTotalDistance();
        this.weightedReindeerWeariness = calcWeightedReindeerWeariness(tripSteps);
        this.id = tourCounter+1;
        tourCounter++;
    }

    public ArrayList<TripStep> getTripSteps() {
        return tripSteps;
    }

    public Sleigh getSleigh() {
        return sleigh;
    }

    //TODO Check if this calculation is correct
    private double calcWeightedReindeerWeariness(ArrayList<TripStep> tripSteps){
        double totalGiftWeight = 0;
        for(TripStep tripStep: tripSteps){
            Gift gift = tripStep.getGift();
            if(gift != null)
                totalGiftWeight += gift.getWeight();
        }
        double weightedReindeerWeariness = 0;
        for(TripStep tripStep: tripSteps){
            Gift gift = tripStep.getGift();
            weightedReindeerWeariness += tripStep.getDistance() * (Sleigh.getBaseWeight() + (totalGiftWeight));
            if(gift != null){
                totalGiftWeight -= gift.getWeight();
            }
        }
        return weightedReindeerWeariness;
    }

    // Sums up the distance of all tripSteps
    private double calcTotalDistance(){
        double totalDistance = 0;
        for(TripStep tripStep: tripSteps){
            totalDistance += tripStep.getDistance();
        }
        return totalDistance;
    }

    public double getDistance() {
        return totalDistance;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public double getWeightedReindeerWeariness() {
        return weightedReindeerWeariness;
    }

    public static int getTourCounter() {
        return tourCounter;
    }

    public int getId() {
        return id;
    }
}
