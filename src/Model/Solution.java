package Model;

import java.util.ArrayList;
import java.util.HashMap;

public class Solution {
    private ArrayList<Trip> trips;
    private double distance;
    private double weight;
    private double weightedReindeerWeariness;
    private HashMap<Integer, Integer> solutionMapping;

    public Solution(ArrayList<Trip> tripList){
        this.trips = tripList;
        this.distance = calcTotalTripsDistance(tripList);
        this.weightedReindeerWeariness = calcTotalWeightedReindeerWeariness(tripList);
        this.solutionMapping = makeSolutionMapping();
        this.weight = getTotalWeight();
    }

    private HashMap<Integer, Integer> makeSolutionMapping(){
        HashMap<Integer, Integer> mapping = new HashMap<>();
        System.out.println();
        for(Trip trip : trips){
            System.out.print("Trip: " + trip.getId() +" Count Gifts:"+ trip.getSleigh().getGifts().size()	+"\t");
            for(Gift gift: trip.getSleigh().getGifts()){
                System.out.print(" "+gift.getId()+",");
                if(!mapping.containsKey(gift.getId())){
                    mapping.put(gift.getId(), trip.getId());
                }else{
                    System.err.println("Invalid Tour - there was a gift used in more than one tour!");
                }
            }
            System.out.println();
        }
        System.out.println();
        return mapping;
    }

    private int getTotalWeight(){
        int weight = 0;
        for(Trip trip : trips){
            for(Gift gift: trip.getSleigh().getGifts()){
                weight += gift.getWeight();
            }
        }
        return weight;
    }

    public int getGiftCount(){
        int counter = 0;
        for(Trip trip : trips){
            counter += trip.getSleigh().getGifts().size();
        }
        return counter;
    }

    // Sums up all distances of all trips.
    private double calcTotalTripsDistance(ArrayList<Trip> tripList){
        double totalSolutionDistance = 0;
        for(Trip trip : tripList){
            totalSolutionDistance += trip.getDistance();
        }
        return totalSolutionDistance;
    }

    // Sums up all weightedReinddeWeariness
    private double calcTotalWeightedReindeerWeariness(ArrayList<Trip> tripList){
        double totalWeightedReeindeer = 0;
        for(Trip trip : tripList){
            totalWeightedReeindeer += trip.getWeightedReindeerWeariness();
        }
        return totalWeightedReeindeer;
    }

    /**
     * Checks if the first and the last point of every tour is the NorthPole.
     */
    public boolean isValid(){
        return hasOnlyValidTours() && hasUsedAllGifts();
    }

    private boolean hasUsedAllGifts(){
        return getGiftCount() == 1000000;
    }

    private boolean hasOnlyValidTours(){
        for(Trip trip : trips){
            int tourSize = trip.getTripSteps().size();
            TripStep startTripStep = trip.getTripSteps().get(0);
            TripStep endTrio = trip.getTripSteps().get(tourSize-1);
            boolean isValidStart = isNorthpole(startTripStep.getOrigin().getLongitude(), startTripStep.getOrigin().getLatitude());
            boolean isValidEnd = isNorthpole(endTrio.getDestination().getLongitude(), endTrio.getDestination().getLatitude());
            if(!isValidStart && !isValidEnd){
                return false;
            }
        }
        return true;
    }

    private boolean isNorthpole(double longtitude, double latitdude){
        return Northpole.NORTHPOLE_LATITUDE == latitdude && Northpole.NORTHPOLE_LONGITUDE == longtitude;
    }

    public ArrayList<Trip> getTrips() {
        return trips;
    }

    public double getWeightedReindeerWeariness() {
        return weightedReindeerWeariness;
    }

    public HashMap<Integer, Integer> getSolutionMapping() {
        return solutionMapping;
    }

    public double getDistance() {
        return distance;
    }

    public double getWeight() {
        return weight;
    }
}
