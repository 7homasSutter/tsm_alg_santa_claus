package Model;

import java.util.ArrayList;

public class Sleigh {
    private static final double BASE_WEIGHT = 10;
    private static final double CARGO_WEIGHT_LIMIT = 1000;
    private ArrayList<Gift> loadedGifts;
    private double weight;

    public Sleigh(){
         this.loadedGifts = new ArrayList<>();
         this.weight = BASE_WEIGHT;
    }

    public Sleigh(ArrayList<Gift> gifts){
        this.loadedGifts = new ArrayList<>(gifts);
        this.weight = BASE_WEIGHT;
        for(Gift gift: loadedGifts){
            this.weight += gift.getWeight();
        }
    }

    /**
     * Add a item to the sleigh if the maximum weight limit is not reached.
     * @param gift which will be added to the loadedGifts
     * @return true if item was added to the sleigh, false if not.
     */
    public Boolean addItem(Gift gift){
        if(hasSpace(gift)){
            this.weight += gift.getWeight();
            return loadedGifts.add(gift);
        }
        return false;
    }

    public int getItemCount(){
        return loadedGifts.size();
    }

    public ArrayList<Gift> getGifts(){
        return loadedGifts;
    }

    // Check if the sleigh is full with the given gift
    public boolean hasSpace(Gift gift){
        return getWeight() + gift.getWeight() <= CARGO_WEIGHT_LIMIT;
    }

    public void removeItem(Gift gift){
        this.weight -= gift.getWeight();
        loadedGifts.remove(gift);
    }

    public Boolean hasItem(Gift gift){
        return loadedGifts.contains(gift);
    }

    public void clearSleigh(){
        this.weight = BASE_WEIGHT;
        loadedGifts.clear();
    }

    public double getWeight() {
        return weight;
    }

    public static double getBaseWeight() {
        return BASE_WEIGHT;
    }
}
