package Model;

import Util.Haversine;

public class TripStep {
    private Location origin;
    private Location destination;
    private double distance;
    private Gift gift;

    public TripStep(Location origin, Location destination, Gift gift){
        this.origin = origin;
        this.destination = destination;
        this.distance = Haversine.distance(origin.getLatitude(),origin.getLongitude(),destination.getLatitude(),destination.getLongitude());
        this.gift = gift;
    }

    public TripStep(Location origin, Location destination){
        this.origin = origin;
        this.destination = destination;
        this.distance = Haversine.distance(origin.getLatitude(),origin.getLongitude(),destination.getLatitude(),destination.getLongitude());
    }

    public Location getOrigin() {
        return origin;
    }

    public void setOrigin(Location origin) {
        this.origin = origin;
    }

    public Location getDestination() {
        return destination;
    }

    public void setDestination(Location destination) {
        this.destination = destination;
    }

    public double getDistance() {
        return distance;
    }

    public Gift getGift() {
        return gift;
    }
}
